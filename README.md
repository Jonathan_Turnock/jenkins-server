# jenkins-server

Basic Jenkins server

Designed for Home or Development Environments where security is not a concern. Jenkins main image does not run as root, this one does

Jenkins Build Server with python subversion and git pre-installed.

Use the optional scripts with the compose package to pre-configure a custom user, setup plugins and add the ssh key to allow connection to other servers

## Installation

Download and run the container in docker, run with the provided docker-compose file if necessary

## Usage

Jenkins is available on port 8080

Python Script on the root volume /onCreate.py will be run once on creation of the image
Python Script on the root volume /onStart.py will be run each time the image is ran

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)