#!groovy
import hudson.model.JDK
import hudson.tools.InstallSourceProperty
import hudson.tools.JDKInstaller
import jenkins.model.Jenkins
import hudson.plugins.gradle.GradleInstaller
import hudson.plugins.gradle.GradleInstallation

boolean j7Required = true
boolean j8Required = true
List<JDK> jdkList = Jenkins.getInstance().getJDKs()
jdkList.each {
    if (it.name == "Java 7") {
        j7Required = false
    }
    if (it.name == "Java 8") {
        j8Required = false
    }
}
if (j7Required) {
    jdkList.add(new JDK("Java 7", "", [new InstallSourceProperty([new JDKInstaller("jdk-7u80-oth-JPR", true)])]))
}

if (j8Required) {
    jdkList.add(new JDK("Java 8", "", [new InstallSourceProperty([new JDKInstaller("jdk-8u202-oth-JPR", true)])]))
}


GradleInstallation[] installations = [
new GradleInstallation("Gradle 1.2", "", [new InstallSourceProperty([new GradleInstaller("1.2")])]),
new GradleInstallation("Gradle 3.5", "", [new InstallSourceProperty([new GradleInstaller("3.5")])])
]

Jenkins.getActiveInstance().getDescriptorByType(GradleInstallation.DescriptorImpl.class).setInstallations(installations)