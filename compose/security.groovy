#!groovy
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.Domain
import com.cloudbees.plugins.credentials.impl.*
import hudson.security.*
import jenkins.model.*
import jenkins.security.s2m.AdminWhitelistRule

def instance = Jenkins.getInstance()

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin", "admin")
instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()

Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)

File idRsaPublicKeyFile = new File('/root/.ssh/id_rsa.pub')
while (!idRsaPublicKeyFile.exists())
    sleep(1)

def domain = Domain.global()
def store = instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
def privateKey = new BasicSSHUserPrivateKey(
        CredentialsScope.GLOBAL,
        UUID.randomUUID().toString(),
        'root',
        new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(idRsaPublicKeyFile.text),
        '',
        'Automatically created by the security.groovy script on container creation'
)
store.addCredentials(domain, privateKey)
instance.save()