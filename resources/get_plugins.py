import requests

response = requests.get("http://admin:admin@localhost:8080/pluginManager/api/json?depth=1")
data = response.json()
with open("plugins.txt", 'w') as pf:
    for plugin in data["plugins"]:
        pf.write("%s:%s\n" % (plugin["shortName"], plugin["version"]))